﻿//                                  ┌∩┐(◣_◢)┌∩┐
//																				\\
// ResourceCache.cs (31/07/2018)												\\
// Autor: Antonio Mateo (.\Moon Antonio) 	antoniomt.moon@gmail.com			\\
// Descripcion:		Ayudante cache Resources.Load aumenta 2x el aumento del		\\
//					rendimiento.												\\
// Fecha Mod:		31/07/2018													\\
// Ultima Mod:		Version Inicial												\\
//******************************************************************************\\

#region Librerias
using UnityEngine;
#endregion

namespace MoonAntonio.Moonlib
{
	/// <summary>
	/// <para>Ayudante cache Resources.Load aumenta 2x el aumento del rendimiento.</para>
	/// </summary>
	sealed class ResourceCache : MonoBehaviourServicios<ResourceCache>
	{

	}
}